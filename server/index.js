const WebSocket = require("ws")
const CommandHelper = require("../server/helpers/CommandHelper");

const cmdHelper = new CommandHelper();
const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', function connection(ws, req) {
    ws.on('message', function incoming(message) {
        message = cmdHelper.getCommand(message)
        console.log(message);
        if(message) {
            console.log('received: ', message);
        }
    });

  ws.send('Hello');
});

const ws = new WebSocket('ws://localhost:8080');

ws.on('open', function open() {
    ws.send('BSCT hello');
});

ws.on('message', function incoming(data) {
    console.log(data);
});