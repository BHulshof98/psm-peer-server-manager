class CommandHelper {
    /**
     * Converts a received message into a command structure.
     * @param {String} command 
     */
    getCommand(command) {
        var contents = command.split(" ");

        if(contents.length > 0) {
            if(this.validCommand(contents[0])) {
                return contents;
            }
        }

        return null;
    }

    /**
     * Checks if the command type is valid.
     * @param {String} commandType 
     */
    validCommand(commandType) {
        var match = false;
        CommandTypes.forEach(element => {
            if(element === commandType) {
                match = true;
            }
        });
        return match;
    }
}

const CommandTypes = ["HELO", "BSCT", "DSCN", "PM", "LST", "BAN", "FDI", "FD", "FUI", "FU", "PONG"]

module.exports = CommandHelper;