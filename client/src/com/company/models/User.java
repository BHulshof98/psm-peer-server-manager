package com.company.models;

public class User {
    private int id;
    private String name;
    private Role role;
    private String managementLocation;

    public User(int id, String name, Role role, String managementLocation) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.managementLocation = managementLocation;
    }

    /**
     * Get the id of the user.
     * @return id of the user.
     */
    public int getId() {
        return id;
    }

    /**
     * Get the name of the user.
     * @return name of the user.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the user.
     * @param name for the user.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the role of the user.
     * @return role of the user.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Set the role of the user.
     * @param role for the user.
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Get the management location path of the user.
     * This will tell the system where to upload the files from,
     * and also where to download the files to.
     * @return management location.
     */
    public String getManagementLocation() {
        return managementLocation;
    }

    /**
     * Set the management location path of the user.
     * This will tell the system where to upload the files from,
     * and also where to download the files to.
     * @param managementLocation for the user.
     */
    public void setManagementLocation(String managementLocation) {
        this.managementLocation = managementLocation;
    }

    /**
     * Get the user tag.
     * @return user tag.
     */
    @Override
    public String toString() {
        return role.toString() + " " + name;
    }
}
