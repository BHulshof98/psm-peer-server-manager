package com.company.models;

public class Role {
    private int id;
    private String name;

    public Role(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Get the id of this role.
     * @return id of this role
     */
    public int getId() {
        return id;
    }

    /**
     * Get the name of this role.
     * @return name of this role
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this role.
     * @param name for this role
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "[" + name + "]";
    }
}
