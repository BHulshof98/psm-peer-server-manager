package com.company.models;

public class Command {
    private String type;
    private String content;

    public Command(String type, String content) {
        this.type = type;
        this.content = content;
    }

    /**
     * Get the type of the command.
     * @return type of the command.
     */
    public String getType() {
        return type;
    }

    /**
     * Get the content of the command.
     * @return content of the command.
     */
    public String getContent() {
        return content;
    }
}
