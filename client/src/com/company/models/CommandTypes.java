package com.company.models;

public enum CommandTypes {
    HELO, // login
    BSCT, // broadcast
    DSCN, // disconnect
    PM, // private message
    LST, // get a list of users
    BAN, // ban a member from the system
    FDI, // file download initialization requested
    FD, // file download execute
    FUI, // file upload initialization requested
    FU, // file upload execution
    PING // receive pong
}
