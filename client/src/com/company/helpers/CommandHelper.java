package com.company.helpers;

import com.company.models.Command;
import com.company.models.CommandTypes;

import java.util.Arrays;

public class CommandHelper {
    /**
     * Get command from client.
     * @param received received string.
     * @return command with type and content.
     */
    public Command getCommand(String received) {
        String[] result = received.split("\\s+");
        String[] content = new String[result.length-1];

        System.arraycopy(result, 1, content, 0, result.length - 1);

        return new Command(result[0], String.join(" ", content));
    }

    /**
     * Get a second layer for a multi-layered command.
     * @param content of the command.
     * @return a second command.
     */
    public String[] getSecondaryContents(String content) {
        String[] newContents = new String[2];
        String[] splitContent = content.split(" ");
        String[] secondary = new String[splitContent.length-1];

        newContents[0] = splitContent[0];

        System.arraycopy(splitContent, 1, secondary, 0, splitContent.length - 1);

        newContents[1] = String.join(" ", secondary);

        return newContents;
    }

    /**
     * Check if the command is valid and exists.
     * @param command to check.
     * @return boolean.
     */
    public boolean isValidCommand(Command command) {
        for(String typeName: getTypeNames(CommandTypes.class)) {
            if(typeName.equals(command.getType()))
                return true;
        }
        return false;
    }

    /**
     * Get all commands.
     * @param e command enumeration class.
     * @return list of command names.
     */
    private String[] getTypeNames(Class<? extends Enum<?>> e) {
        return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }
}
