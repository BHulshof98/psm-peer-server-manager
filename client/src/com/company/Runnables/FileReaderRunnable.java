package com.company.Runnables;

import com.company.helpers.CommandHelper;
import com.company.models.Command;
import com.company.models.User;

import java.io.IOException;
import java.io.InputStream;

public class FileReaderRunnable implements Runnable {
    private int fileSize;
    private String fileName;
    private byte[] received;
    private InputStream inputStream;
    private boolean isActive;
    private CommandHelper commandHelper;
    private User user;

    public FileReaderRunnable(InputStream inputStream, User user) {
        fileSize = 0;
        fileName = null;
        this.inputStream = inputStream;
        isActive = true;
        commandHelper = new CommandHelper();
        this.user = user;
    }

    @Override
    public void run() {
        while(isActive) {
            byte[] buffer;

            try {
                buffer = new byte[inputStream.available()];

                while (inputStream.available() > 0) {
                    int read = inputStream.read(buffer);
                    if(read == 0)
                        break;
                }

                if(buffer.length > 0) {
                    if(fileSize == 0) {
                        handleInput(new String(buffer));
                    }
                } else {
                    received = buffer;

                    // TODO: save file with data.

                    // reset data.
                    fileSize = 0;
                    fileName = null;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Set up file download data.
     * @param fileSize size of file.
     * @param fileName name of file.
     */
    private void setup(int fileSize, String fileName) {
        this.fileSize = fileSize;
        this.fileName = fileName;
    }

    /**
     * Handle input.
     * In this class it only happens when the file size has not been set.
     * If the file size has been set it means that we only have to read byte data and save it.
     * @param message received.
     */
    private void handleInput(String message) {
        Command command = commandHelper.getCommand(message);

        if ("FDI".equals(command.getType())) {
            String[] contents = commandHelper.getSecondaryContents(command.getContent());
            setup(Integer.valueOf(contents[0]), contents[1]);
        }
    }
}
